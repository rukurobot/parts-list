# README #

This Repository is for access to the Ruku Robot 3D modeled parts.

# Quick summary #

* There are 6 parts in total.
* The suffix ALU is meant for Aluminum parts.
* The suffix PLA is meant for 3D printed parts.
* * Parts labeled 'Claw' and 'Pinch motor shaft' should be printed in ABS.
* * Parts labeled 'Pinch motor mount', 'Main bracket', 'Small angle bracket', and 'Pinch arm' should be printed in PLA.
* * Parts that can be printed in the four different colors of the cube are: 'Main bracket', 'Claw', 'Small angle bracket', and 'Pinch arm'

# Single Build #
To build a single robot you'll need parts in the following quantities.

* Claw - 4
* Main bracket - 4
* Pinch arm - 4
* Pinch motor mount - 2
* Pinch motor shaft -  2
* Small angle bracket - 4